@file:Suppress("UnstableApiUsage")

pluginManagement {
    includeBuild("build-logic")

    repositories {
        mavenLocal()
        maven("https://maven.toastbits.dev/")

        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositories {
        mavenLocal()
        maven("https://maven.toastbits.dev/")

        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

rootProject.name = "kmp-template"
include(":library")
include(":application")
